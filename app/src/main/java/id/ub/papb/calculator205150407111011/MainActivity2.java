package id.ub.papb.calculator205150407111011;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    private TextView tvResult2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        String result = getIntent().getExtras().getString("result");

        tvResult2 = findViewById(R.id.tvResult2);
        tvResult2.setText(result);
    }
}