package id.ub.papb.calculator205150407111011;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button zero, one, two, three, four, five, six, seven, eight, nine, divide, multiply, add, subtract, clear, equal;
    private TextView tvResult, tvToCalculate;
    private final char ADDITION = '+';
    private final char SUBTRACTION = '-';
    private final char MULTIPLICATION = '*';
    private final char DIVISION = '/';
    private final char EQU = 0;
    private double val1 = Double.NaN;
    private double val2;
    private char ACTION;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupUIViews();

        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvToCalculate.setText(tvToCalculate.getText().toString() + "0");
            }
        });

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvToCalculate.setText(tvToCalculate.getText().toString() + "1");
            }
        });

        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvToCalculate.setText(tvToCalculate.getText().toString() + "2");
            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvToCalculate.setText(tvToCalculate.getText().toString() + "3");
            }
        });

        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvToCalculate.setText(tvToCalculate.getText().toString() + "4");
            }
        });

        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvToCalculate.setText(tvToCalculate.getText().toString() + "5");
            }
        });

        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvToCalculate.setText(tvToCalculate.getText().toString() + "6");
            }
        });

        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvToCalculate.setText(tvToCalculate.getText().toString() + "7");
            }
        });

        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvToCalculate.setText(tvToCalculate.getText().toString() + "8");
            }
        });

        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvToCalculate.setText(tvToCalculate.getText().toString() + "9");
            }
        });



        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                compute();
                ACTION = ADDITION;
                tvResult.setText(String.valueOf(val1) + "+");
                tvToCalculate.setText(null);

            }
        });

        subtract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                compute();
                ACTION = SUBTRACTION;
                tvResult.setText(String.valueOf(val1) + "-");
                tvToCalculate.setText(null);
            }
        });

        multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                compute();
                ACTION = MULTIPLICATION;
                tvResult.setText(String.valueOf(val1) + "x");
                tvToCalculate.setText(null);
            }
        });

        divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                compute();
                ACTION = DIVISION;
                tvResult.setText(String.valueOf(val1) + "/");
                tvToCalculate.setText(null);
            }
        });

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tvToCalculate.getText().length() > 0) {
                    CharSequence name = tvToCalculate.getText().toString();
                    tvToCalculate.setText(name.subSequence(0, name.length()-1));
                } else {
                    val1 = Double.NaN;
                    val2 = Double.NaN;
                    tvToCalculate.setText(null);
                    tvResult.setText(null);
                }
            }
        });

        equal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                compute();
                ACTION = EQU;
                String resultVar = tvResult.getText().toString() + String.valueOf(val2) + "=" + String.valueOf(val2);
//                tvResult.setText(resultVar);
                tvToCalculate.setText(null);

                Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                intent.putExtra("result", resultVar);
                startActivity(intent);
            }
        });
    }

    private void setupUIViews() {
        one = (Button) findViewById(R.id.bt1);
        two = (Button) findViewById(R.id.bt2);
        three = (Button) findViewById(R.id.bt3);
        four = (Button) findViewById(R.id.bt4);
        five = (Button) findViewById(R.id.bt5);
        six = (Button) findViewById(R.id.bt6);
        seven = (Button) findViewById(R.id.bt7);
        eight = (Button) findViewById(R.id.bt8);
        nine = (Button) findViewById(R.id.bt9);
        zero = (Button) findViewById(R.id.bt0);
        divide = (Button) findViewById(R.id.btDivide);
        multiply = (Button) findViewById(R.id.btMultiply);
        add = (Button) findViewById(R.id.btPlus);
        subtract = (Button) findViewById(R.id.btMinus);
        clear = (Button) findViewById(R.id.btClear);
        equal = (Button) findViewById(R.id.btEqual);
        tvResult = (TextView) findViewById(R.id.tvResult);
        tvToCalculate = (TextView) findViewById(R.id.tvToCalculate);
    }

    private void compute() {
        if(!Double.isNaN(val1)) {
            val2 = Double.parseDouble(tvToCalculate.getText().toString());

            switch(ACTION) {
                case ADDITION:
                    val1 = val1 + val2;
                    break;
                case SUBTRACTION:
                    val1 = val1 - val2;
                    break;
                case MULTIPLICATION:
                    val1 = val1 * val2;
                    break;
                case DIVISION:
                    val1 = val1 / val2;
                    break;
                case EQU:
                    break;
            }
        } else {
            val1 = Double.parseDouble(tvToCalculate.getText().toString());
        }
    }
}